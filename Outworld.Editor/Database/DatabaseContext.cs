using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Outworld.Editor.Database.Entities;

// ReSharper disable InconsistentNaming

#pragma warning disable 8618

namespace Outworld.Editor.Database
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class DatabaseContext : DbContext
    {
        public DbSet<Item> Items { get; set; }
        public DbSet<ItemType> ItemTypes { get; set; }
        public DbSet<ItemCategory> ItemCategories { get; set; }
        public DbSet<ItemFlag> ItemFlags { get; set; }
        public DbSet<ItemAffix> ItemAffixes { get; set; }
        public DbSet<ItemItemFlag> ItemItemFlags { get; set; }

        public DbSet<Rarity> Rarities { get; set; }

        public DbSet<Property> Properties { get; set; }
        public DbSet<PropertyModifier> PropertyModifiers { get; set; }

        public DbSet<Weapon> Weapons { get; set; }
        public DbSet<Projectile> Projectiles { get; set; }
        public DbSet<Effect> Effects { get; set; }
        public DbSet<EffectEffect> EffectEffects { get; set; }
        public DbSet<EffectBehaviourFlag> EffectBehaviourFlags { get; set; }

        public DbSet<I18n> I18n { get; set; }
        public DbSet<I18nEffect> I18nEffects { get; set; }

        public DbSet<ItemCategoryItemType> ItemCategoryItemTypes { get; set; }
        public DbSet<ItemAffixPropertyModifier> ItemAffixPropertyModifiers { get; set; }
        public DbSet<Archetype> Archetypes { get; set; }
        public DbSet<ArchetypePropertyModifier> ArchetypePropertyModifiers { get; set; }
        public DbSet<Behaviour> Behaviours { get; set; }
        public DbSet<BehaviourBehaviour> BehaviourBehaviours { get; set; }
        public DbSet<BehaviourPropertyModifier> BehaviourPropertyModifiers { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<UnitUnitFlag> UnitUnitFlags { get; set; }
        public DbSet<UnitBehaviour> UnitBehaviours { get; set; }
        public DbSet<UnitFlag> UnitFlags { get; set; }
        public DbSet<Validator> Validators { get; set; }
        public DbSet<ValidatorValidator> ValidatorValidators { get; set; }

        public DbSet<LootTable> LootTables { get; set; }
        public DbSet<LootTableItem> LootTableItems { get; set; }
        public DbSet<LootTableRandomItem> LootTableRandomItems { get; set; }
        public DbSet<LootTableLootTable> LootTableLootTables { get; set; }

        public DbSet<StatusFlag> StatusFlags { get; set; }
        public DbSet<BehaviourFlag> BehaviourFlags { get; set; }

        public DbSet<ValidatorStatusFlag> ValidatorStatusFlags { get; set; }
        public DbSet<ValidatorUnitFlag> ValidatorUnitFlags { get; set; }
        public DbSet<BehaviourBehaviourFlag> BehaviourBehaviourFlags { get; set; }
        public DbSet<BehaviourStatusFlag> BehaviourStatusFlags { get; set; }

        public DbSet<Configuration> Configuration { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            Property.OnModelCreating(builder);
            Weapon.OnModelCreating(builder);
            Effect.OnModelCreating(builder);
            Behaviour.OnModelCreating(builder);
            LootTable.OnModelCreating(builder);
            Validator.OnModelCreating(builder);
        }
    }
}