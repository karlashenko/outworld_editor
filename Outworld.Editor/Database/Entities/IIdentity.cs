namespace Outworld.Editor.Database.Entities
{
    public interface IIdentity
    {
        public int Id { get; set; }
    }
}