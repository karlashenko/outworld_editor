using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class Projectile : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public string? Name { get; set; }
        public string? Prefab { get; set; }
        public string? ImpactPrefab { get; set; }
        public float Speed { get; set; }
        public bool DestroyOnCollision { get; set; }
        public bool RicochetOnCollision { get; set; }
        public bool IsHoming { get; set; }
        public float HomingRadius { get; set; }
        public float HomingSpeed { get; set; }
    }
}