using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class ItemItemFlag
    {
        [Key]
        public int Id { get; set; }

        public Item Item { get; set; }
        public int ItemId { get; set; }

        public ItemFlag ItemFlag { get; set; }
        public int ItemFlagId { get; set; }
    }
}