using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Outworld.Editor.Database.Entities
{
    public class LootTable : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public string? Name { get; set; }
        public int Count { get; set; }

        public List<LootTableItem> Items { get; set; } = new();

        public List<LootTableRandomItem> RandomItems { get; set; } = new();

        public List<LootTableLootTable> LootTables { get; set; } = new();

        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<LootTableLootTable>()
                .HasOne(related => related.Parent)
                .WithMany(lootTable => lootTable.LootTables)
                .HasForeignKey(related => related.ParentId);
        }
    }
}