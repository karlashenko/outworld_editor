using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Outworld.Editor.Database.Entities
{
    public class Weapon : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public string? Name { get; set; }
        public string? Type { get; set; }
        public string? Prefab { get; set; }
        public float Range { get; set; }
        public float CooldownTime { get; set; }
        public float PreparationTime { get; set; }
        public bool IsCharging { get; set; }
        public float ChargeDurationSeconds { get; set; }
        public Effect? Effect { get; set; }
        public int? EffectId { get; set; }
        public Projectile? Projectile { get; set; }
        public int? ProjectileId { get; set; }

        public string? OriginParticlesPrefab { get; set; }
        public string? TargetParticlesPrefab { get; set; }

        public string? BeamPrefab { get; set; }
        public int BeamReflectionCount { get; set; }

        public int ShotSpreadCount { get; set; }
        public float ShotSpreadDegrees { get; set; }
        public int ShotBarrageCount { get; set; }
        public float ShotBarrageInterval { get; set; }

        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Weapon>().HasOne(model => model.Effect);
            builder.Entity<Weapon>().HasOne(model => model.Projectile);
        }
    }
}