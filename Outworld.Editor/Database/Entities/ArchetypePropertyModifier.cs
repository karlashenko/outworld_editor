using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class ArchetypePropertyModifier
    {
        [Key]
        public int Id { get; set; }

        public int ArchetypeId { get; set; }
        public Archetype Archetype { get; set; }

        public int PropertyModifierId { get; set; }
        public PropertyModifier PropertyModifier { get; set; }
    }
}