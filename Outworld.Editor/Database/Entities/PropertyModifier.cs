using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class PropertyModifier : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public string? Name { get; set; }

        public Property? Property { get; set; }
        public int? PropertyId { get; set; }

        public string? Type { get; set; }
        public float Amount { get; set; } = 0;
    }
}