using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class UnitBehaviour
    {
        [Key]
        public int Id { get; set; }

        public Unit Unit { get; set; }
        public int UnitId { get; set; }

        public Behaviour Behaviour { get; set; }
        public int BehaviourId { get; set; }
    }
}