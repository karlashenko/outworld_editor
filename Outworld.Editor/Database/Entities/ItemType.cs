using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class ItemType : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public I18n? Name { get; set; }
        public int? NameId { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }
    }
}