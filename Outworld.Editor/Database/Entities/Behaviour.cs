using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Outworld.Editor.Database.Entities
{
    public class Behaviour : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public I18n? Name { get; set; }
        public int? NameId { get; set; }

        public I18n? Description { get; set; }
        public int? DescriptionId { get; set; }

        public bool IsPositive { get; set; }
        public string? Icon { get; set; }
        public string? Type { get; set; }
        public float Duration { get; set; } = 0;
        public int StackCount { get; set; } = 0;
        public List<BehaviourBehaviourFlag> Flags { get; set; } = new();
        public List<BehaviourStatusFlag> StatusFlags { get; set; } = new();

        public float BuffTickPeriod { get; set; }
        public int? BuffApplyEffectId { get; set; }
        public int? BuffTickEffectId { get; set; }
        public int? BuffExpireEffectId { get; set; }
        public int? BuffRemoveEffectId { get; set; }
        public Effect? BuffApplyEffect { get; set; }
        public Effect? BuffTickEffect { get; set; }
        public Effect? BuffExpireEffect { get; set; }
        public Effect? BuffRemoveEffect { get; set; }

        public Effect? OnTakeDamageEffect { get; set; }
        public int? OnTakeDamageEffectId { get; set; }

        public Effect? OnDealDamageEffect { get; set; }
        public int? OnDealDamageEffectId { get; set; }

        public Effect? OnKillEffect { get; set; }
        public int? OnKillEffectId { get; set; }

        public Effect? OnDeathEffect { get; set; }
        public int? OnDeathEffectId { get; set; }

        public float AuraRadius { get; set; }
        public Behaviour? AuraBehaviour { get; set; }
        public int? AuraBehaviourId { get; set; }
        public Validator? AuraValidator { get; set; }
        public int? AuraValidatorId { get; set; }

        public List<BehaviourPropertyModifier> PropertyModifiers { get; set; } = new();

        public List<BehaviourBehaviour> Behaviours { get; set; } = new();

        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Behaviour>().HasOne(model => model.BuffApplyEffect);
            builder.Entity<Behaviour>().HasOne(model => model.BuffExpireEffect);
            builder.Entity<Behaviour>().HasOne(model => model.BuffRemoveEffect);
            builder.Entity<Behaviour>().HasOne(model => model.BuffTickEffect);
            builder.Entity<Behaviour>().HasOne(model => model.OnDeathEffect);
            builder.Entity<Behaviour>().HasOne(model => model.OnKillEffect);
            builder.Entity<Behaviour>().HasOne(model => model.OnDealDamageEffect);
            builder.Entity<Behaviour>().HasOne(model => model.OnTakeDamageEffect);

            builder.Entity<Behaviour>()
                .HasOne(model => model.AuraValidator)
                .WithOne()
                .HasForeignKey<Behaviour>(validator => validator.AuraValidatorId);

            builder.Entity<Behaviour>()
                .HasOne(model => model.AuraBehaviour)
                .WithOne()
                .HasForeignKey<Behaviour>(behaviour => behaviour.AuraBehaviourId);

            builder.Entity<BehaviourBehaviour>()
                .HasOne(related => related.Parent)
                .WithMany(behaviour => behaviour.Behaviours)
                .HasForeignKey(related => related.ParentId);
        }
    }
}