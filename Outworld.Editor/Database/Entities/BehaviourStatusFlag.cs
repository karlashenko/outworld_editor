using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class BehaviourStatusFlag
    {
        [Key]
        public int Id { get; set; }

        public Behaviour Behaviour { get; set; }
        public int BehaviourId { get; set; }

        public StatusFlag StatusFlag { get; set; }
        public int StatusFlagId { get; set; }
    }
}