using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class StatusFlag : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public string? Name { get; set; }
    }
}