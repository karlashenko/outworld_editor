using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class ItemCategoryItemType
    {
        [Key]
        public int Id { get; set; }

        public ItemCategory ItemCategory { get; set; }
        public int ItemCategoryId { get; set; }

        public ItemType ItemType { get; set; }
        public int ItemTypeId { get; set; }
    }
}