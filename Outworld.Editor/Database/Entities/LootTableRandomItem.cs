using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class LootTableRandomItem
    {
        [Key]
        public int Id { get; set; }

        public LootTable LootTable { get; set; }
        public int LootTableId { get; set; }

        public ItemCategory? ItemCategory { get; set; }
        public int? ItemCategoryId { get; set; }

        public Rarity? Rarity { get; set; }
        public int? RarityId { get; set; }

        public int Weight { get; set; }
        public bool IsGuaranteed { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsUnique { get; set; }
    }
}