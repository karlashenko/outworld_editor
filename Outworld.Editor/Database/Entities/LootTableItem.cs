using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class LootTableItem
    {
        [Key]
        public int Id { get; set; }

        public LootTable LootTable { get; set; }
        public int LootTableId { get; set; }

        public Item Item { get; set; }
        public int ItemId { get; set; }

        public int StackMin { get; set; }
        public int StackMax { get; set; }

        public int Weight { get; set; }
        public bool IsGuaranteed { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsUnique { get; set; }
    }
}