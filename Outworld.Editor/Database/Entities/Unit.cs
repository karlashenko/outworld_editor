using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class Unit : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public I18n? Name { get; set; }
        public int? NameId { get; set; }

        public Archetype? Archetype { get; set; }
        public int? ArchetypeId { get; set; }

        public int Tier { get; set; }
        public string? Prefab { get; set; }

        public List<UnitUnitFlag> Flags { get; set; } = new();

        public List<UnitBehaviour> Behaviours { get; set; } = new();
    }
}