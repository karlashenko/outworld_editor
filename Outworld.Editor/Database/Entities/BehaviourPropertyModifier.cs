using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class BehaviourPropertyModifier
    {
        [Key]
        public int Id { get; set; }

        public int BehaviourId { get; set; }
        public Behaviour Behaviour { get; set; }

        public int PropertyModifierId { get; set; }
        public PropertyModifier PropertyModifier { get; set; }
    }
}