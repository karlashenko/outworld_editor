using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class UnitUnitFlag
    {
        [Key]
        public int Id { get; set; }

        public Unit Unit { get; set; }
        public int UnitId { get; set; }

        public UnitFlag UnitFlag { get; set; }
        public int UnitFlagId { get; set; }
    }
}