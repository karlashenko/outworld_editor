using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class ItemCategory : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public I18n? Name { get; set; }
        public int? NameId { get; set; }

        public List<ItemCategoryItemType> Types { get; set; } = new();
    }
}