namespace Outworld.Editor.Database.Entities
{
    public class PropertyContainer
    {
        public object this[string propertyName] => GetType().GetProperty(propertyName).GetValue(this, null);
    }
}