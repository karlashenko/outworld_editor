using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Outworld.Editor.Database.Entities
{
    public class Validator : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public string? Name { get; set; }
        public string? Type { get; set; }
        public string? Comparator { get; set; }
        public float? Value { get; set; }

        public Unit? Unit { get; set; }
        public int? UnitId { get; set; }

        public Behaviour? Behaviour { get; set; }
        public int? BehaviourId { get; set; }

        public List<ValidatorStatusFlag> StatusFlags { get; set; } = new();

        public List<ValidatorUnitFlag> UnitFlags { get; set; } = new();

        public List<ValidatorValidator> Validators { get; set; } = new();

        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Validator>().HasOne(model => model.Behaviour);
            builder.Entity<Validator>().HasOne(model => model.Unit);

            builder.Entity<ValidatorValidator>()
                .HasOne(related => related.Parent)
                .WithMany(validator => validator.Validators)
                .HasForeignKey(related => related.ParentId);
        }
    }
}