using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class SkillSkillTags
    {
        [Key]
        public int Id { get; set; }

        public Skill Skill { get; set; }
        public int SkillId { get; set; }

        public SkillTag SkillTag { get; set; }
        public int SkillTagId { get; set; }
    }
}