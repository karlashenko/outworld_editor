using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class Item : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public I18n? Name { get; set; }
        public int? NameId { get; set; }

        public I18n? Description { get; set; }
        public int? DescriptionId { get; set; }

        public string? Icon { get; set; }

        public ItemType? ItemType { get; set; }
        public int? ItemTypeId { get; set; }

        public Rarity? Rarity { get; set; }
        public int? RarityId { get; set; }

        public List<ItemItemFlag> Flags { get; set; } = new();
    }
}