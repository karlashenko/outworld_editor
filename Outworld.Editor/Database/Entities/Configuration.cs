using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class Configuration : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }
    }
}