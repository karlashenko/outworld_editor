using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class ValidatorStatusFlag
    {
        [Key]
        public int Id { get; set; }

        public Validator Validator { get; set; }
        public int ValidatorId { get; set; }

        public StatusFlag StatusFlag { get; set; }
        public int StatusFlagId { get; set; }
    }
}