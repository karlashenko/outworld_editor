using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class Skill : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public I18n? Name { get; set; }
        public int? NameId { get; set; }

        public I18n? Description { get; set; }
        public int? DescriptionId { get; set; }

        public Effect? Effect { get; set; }
        public int? EffectId { get; set; }

        public string? Icon { get; set; }

        public List<SkillSkillTags> Tags { get; set; } = new();

        public List<SkillSkillFlags> Flags { get; set; } = new();
    }
}