using System.ComponentModel.DataAnnotations;

// ReSharper disable InconsistentNaming

namespace Outworld.Editor.Database.Entities
{
    public class I18nEffect
    {
        [Key]
        public int Id { get; set; }

        public I18n I18n { get; set; }
        public int I18nId { get; set; }

        public Effect Effect { get; set; }
        public int EffectId { get; set; }

        public string? Placeholder { get; set; }
    }
}