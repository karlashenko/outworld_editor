using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class ItemAffix : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public I18n? Name { get; set; }
        public int? NameId { get; set; }

        public I18n? Description { get; set; }
        public int? DescriptionId { get; set; }

        public string? Type { get; set; }
        public int Weight { get; set; } = 0;

        public Behaviour? Behaviour { get; set; }
        public int? BehaviourId { get; set; }

        public List<ItemAffixPropertyModifier> PropertyModifiers { get; set; } = new();
    }
}