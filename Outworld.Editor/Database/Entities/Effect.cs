using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Outworld.Editor.Database.Entities
{
    public class Effect : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public string? Name { get; set; }
        public string? Type { get; set; }
        public string? OriginParticlesPrefab { get; set; }
        public string? TargetParticlesPrefab { get; set; }
        public bool IsImmediate { get; set; }

        public string? DamageType { get; set; }
        public string? DamageFormula { get; set; }

        public int? ApplyBehaviourStackCount { get; set; }
        public Behaviour? ApplyBehaviourBehaviour { get; set; }
        public int? ApplyBehaviourBehaviourId { get; set; }

        public List<EffectBehaviourFlag> RemoveBehaviourFlags { get; set; } = new();
        public Behaviour? RemoveBehaviourBehaviour { get; set; }
        public int? RemoveBehaviourBehaviourId { get; set; }

        public Projectile? Projectile { get; set; }
        public int? ProjectileId { get; set; }

        public Effect? ProjectileEffect { get; set; }
        public int? ProjectileEffectId { get; set; }

        public int? SearchAreaTargetLimit { get; set; }
        public float? SearchAreaRadius { get; set; }
        public Validator? SearchAreaValidator { get; set; }
        public int? SearchAreaValidatorId { get; set; }
        public Effect? SearchAreaEffect { get; set; }
        public int? SearchAreaEffectId { get; set; }

        public List<EffectEffect> Effects { get; set; } = new();

        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Effect>().HasOne(model => model.ApplyBehaviourBehaviour);
            builder.Entity<Effect>().HasOne(model => model.RemoveBehaviourBehaviour);

            builder.Entity<Effect>()
                .HasOne(model => model.ProjectileEffect)
                .WithOne()
                .HasForeignKey<Effect>(effect => effect.ProjectileEffectId);

            builder.Entity<Effect>()
                .HasOne(model => model.SearchAreaEffect)
                .WithOne()
                .HasForeignKey<Effect>(effect => effect.SearchAreaEffectId);

            builder.Entity<EffectEffect>()
                .HasOne(related => related.Parent)
                .WithMany(effect => effect.Effects)
                .HasForeignKey(related => related.ParentId);
        }
    }
}