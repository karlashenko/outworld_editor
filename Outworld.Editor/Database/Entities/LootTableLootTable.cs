using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class LootTableLootTable
    {
        [Key]
        public int Id { get; set; }

        public LootTable Parent { get; set; }
        public int ParentId { get; set; }

        public LootTable Related { get; set; }
        public int RelatedId { get; set; }

        public int Weight { get; set; }
        public bool IsGuaranteed { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsUnique { get; set; }
    }
}