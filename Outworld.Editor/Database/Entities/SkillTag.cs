using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class SkillTag : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public I18n? Name { get; set; }
        public int? NameId { get; set; }
    }
}