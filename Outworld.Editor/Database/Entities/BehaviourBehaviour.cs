using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class BehaviourBehaviour
    {
        [Key]
        public int Id { get; set; }

        public Behaviour Parent { get; set; }
        public int ParentId { get; set; }

        public Behaviour Related { get; set; }
        public int RelatedId { get; set; }
    }
}