using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class EffectBehaviourFlag
    {
        [Key]
        public int Id { get; set; }

        public Effect Effect { get; set; }
        public int EffectId { get; set; }

        public BehaviourFlag BehaviourFlag { get; set; }
        public int BehaviourFlagId { get; set; }
    }
}