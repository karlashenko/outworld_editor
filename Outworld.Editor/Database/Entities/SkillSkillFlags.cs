using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class SkillSkillFlags
    {
        [Key]
        public int Id { get; set; }

        public Skill Skill { get; set; }
        public int SkillId { get; set; }

        public SkillFlag SkillFlag { get; set; }
        public int SkillFlagId { get; set; }
    }
}