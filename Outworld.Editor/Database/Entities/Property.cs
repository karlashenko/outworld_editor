using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Outworld.Editor.Database.Entities
{
    public class Property : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public string? Type { get; set; }

        public I18n? Name { get; set; }
        public int? NameId { get; set; }

        public I18n? Description { get; set; }
        public int? DescriptionId { get; set; }

        public float Min { get; set; }
        public float Max { get; set; }

        public static void OnModelCreating(ModelBuilder builder)
        {
        }
    }
}