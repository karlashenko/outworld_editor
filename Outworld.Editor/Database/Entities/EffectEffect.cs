using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class EffectEffect
    {
        [Key]
        public int Id { get; set; }

        public Effect Parent { get; set; }
        public int ParentId { get; set; }

        public Effect Related { get; set; }
        public int RelatedId { get; set; }
    }
}