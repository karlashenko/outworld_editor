using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class BehaviourBehaviourFlag
    {
        [Key]
        public int Id { get; set; }

        public Behaviour Behaviour { get; set; }
        public int BehaviourId { get; set; }

        public BehaviourFlag BehaviourFlag { get; set; }
        public int BehaviourFlagId { get; set; }
    }
}