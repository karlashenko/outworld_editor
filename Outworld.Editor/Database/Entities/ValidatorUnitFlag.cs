using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class ValidatorUnitFlag
    {
        [Key]
        public int Id { get; set; }

        public Validator Validator { get; set; }
        public int ValidatorId { get; set; }

        public UnitFlag UnitFlag { get; set; }
        public int UnitFlagId { get; set; }
    }
}