using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class ItemAffixPropertyModifier
    {
        [Key]
        public int Id { get; set; }

        public ItemAffix ItemAffix { get; set; }
        public int ItemAffixId { get; set; }

        public PropertyModifier PropertyModifier { get; set; }
        public int PropertyModifierId { get; set; }
    }
}