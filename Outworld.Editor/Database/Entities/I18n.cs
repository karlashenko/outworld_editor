using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    // ReSharper disable once InconsistentNaming
    public class I18n : PropertyContainer, IIdentity
    {
        [Key]
        public int Id { get; set; }

        public string? Key { get; set; }
        public string? Ru { get; set; }
        public string? En { get; set; }

        public List<I18nEffect> Effects { get; set; } = new();

        public override string ToString()
        {
            return En ?? "";
        }
    }
}