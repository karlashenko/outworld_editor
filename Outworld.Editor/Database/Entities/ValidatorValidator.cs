using System.ComponentModel.DataAnnotations;

namespace Outworld.Editor.Database.Entities
{
    public class ValidatorValidator
    {
        [Key]
        public int Id { get; set; }

        public Validator Parent { get; set; }
        public int ParentId { get; set; }

        public Validator Related { get; set; }
        public int RelatedId { get; set; }
    }
}