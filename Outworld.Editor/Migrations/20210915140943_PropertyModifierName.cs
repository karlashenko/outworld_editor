﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Outworld.Editor.Migrations
{
    public partial class PropertyModifierName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "PropertyModifiers",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "PropertyModifiers");
        }
    }
}
