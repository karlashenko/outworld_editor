﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Outworld.Editor.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BehaviourFlags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BehaviourFlags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Configuration",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Configuration", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "I18n",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Key = table.Column<string>(type: "text", nullable: true),
                    Ru = table.Column<string>(type: "text", nullable: true),
                    En = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_I18n", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ItemFlags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemFlags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LootTables",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Count = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LootTables", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Projectiles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Prefab = table.Column<string>(type: "text", nullable: true),
                    ImpactPrefab = table.Column<string>(type: "text", nullable: true),
                    Speed = table.Column<float>(type: "real", nullable: false),
                    DestroyOnCollision = table.Column<bool>(type: "boolean", nullable: false),
                    RicochetOnCollision = table.Column<bool>(type: "boolean", nullable: false),
                    IsHoming = table.Column<bool>(type: "boolean", nullable: false),
                    HomingRadius = table.Column<float>(type: "real", nullable: false),
                    HomingSpeed = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projectiles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SkillFlag",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SkillFlag", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StatusFlags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatusFlags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UnitFlags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnitFlags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Archetypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NameId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Archetypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Archetypes_I18n_NameId",
                        column: x => x.NameId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ItemCategories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NameId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemCategories_I18n_NameId",
                        column: x => x.NameId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ItemTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NameId = table.Column<int>(type: "integer", nullable: true),
                    Width = table.Column<int>(type: "integer", nullable: false),
                    Height = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemTypes_I18n_NameId",
                        column: x => x.NameId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Properties",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Type = table.Column<string>(type: "text", nullable: true),
                    NameId = table.Column<int>(type: "integer", nullable: true),
                    DescriptionId = table.Column<int>(type: "integer", nullable: true),
                    Min = table.Column<float>(type: "real", nullable: false),
                    Max = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Properties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Properties_I18n_DescriptionId",
                        column: x => x.DescriptionId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Properties_I18n_NameId",
                        column: x => x.NameId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Rarities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NameId = table.Column<int>(type: "integer", nullable: true),
                    Color = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rarities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Rarities_I18n_NameId",
                        column: x => x.NameId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SkillTag",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NameId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SkillTag", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SkillTag_I18n_NameId",
                        column: x => x.NameId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LootTableLootTables",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ParentId = table.Column<int>(type: "integer", nullable: false),
                    RelatedId = table.Column<int>(type: "integer", nullable: false),
                    Weight = table.Column<int>(type: "integer", nullable: false),
                    IsGuaranteed = table.Column<bool>(type: "boolean", nullable: false),
                    IsEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    IsUnique = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LootTableLootTables", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LootTableLootTables_LootTables_ParentId",
                        column: x => x.ParentId,
                        principalTable: "LootTables",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LootTableLootTables_LootTables_RelatedId",
                        column: x => x.RelatedId,
                        principalTable: "LootTables",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NameId = table.Column<int>(type: "integer", nullable: true),
                    ArchetypeId = table.Column<int>(type: "integer", nullable: true),
                    Tier = table.Column<int>(type: "integer", nullable: false),
                    Prefab = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Units_Archetypes_ArchetypeId",
                        column: x => x.ArchetypeId,
                        principalTable: "Archetypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Units_I18n_NameId",
                        column: x => x.NameId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ItemCategoryItemTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ItemCategoryId = table.Column<int>(type: "integer", nullable: false),
                    ItemTypeId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemCategoryItemTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemCategoryItemTypes_ItemCategories_ItemCategoryId",
                        column: x => x.ItemCategoryId,
                        principalTable: "ItemCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ItemCategoryItemTypes_ItemTypes_ItemTypeId",
                        column: x => x.ItemTypeId,
                        principalTable: "ItemTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PropertyModifiers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PropertyId = table.Column<int>(type: "integer", nullable: true),
                    Type = table.Column<string>(type: "text", nullable: true),
                    Amount = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyModifiers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PropertyModifiers_Properties_PropertyId",
                        column: x => x.PropertyId,
                        principalTable: "Properties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NameId = table.Column<int>(type: "integer", nullable: true),
                    DescriptionId = table.Column<int>(type: "integer", nullable: true),
                    Icon = table.Column<string>(type: "text", nullable: true),
                    ItemTypeId = table.Column<int>(type: "integer", nullable: true),
                    RarityId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items_I18n_DescriptionId",
                        column: x => x.DescriptionId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Items_I18n_NameId",
                        column: x => x.NameId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Items_ItemTypes_ItemTypeId",
                        column: x => x.ItemTypeId,
                        principalTable: "ItemTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Items_Rarities_RarityId",
                        column: x => x.RarityId,
                        principalTable: "Rarities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LootTableRandomItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    LootTableId = table.Column<int>(type: "integer", nullable: false),
                    ItemCategoryId = table.Column<int>(type: "integer", nullable: true),
                    RarityId = table.Column<int>(type: "integer", nullable: true),
                    Weight = table.Column<int>(type: "integer", nullable: false),
                    IsGuaranteed = table.Column<bool>(type: "boolean", nullable: false),
                    IsEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    IsUnique = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LootTableRandomItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LootTableRandomItems_ItemCategories_ItemCategoryId",
                        column: x => x.ItemCategoryId,
                        principalTable: "ItemCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LootTableRandomItems_LootTables_LootTableId",
                        column: x => x.LootTableId,
                        principalTable: "LootTables",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LootTableRandomItems_Rarities_RarityId",
                        column: x => x.RarityId,
                        principalTable: "Rarities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UnitUnitFlags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UnitId = table.Column<int>(type: "integer", nullable: false),
                    UnitFlagId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnitUnitFlags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UnitUnitFlags_UnitFlags_UnitFlagId",
                        column: x => x.UnitFlagId,
                        principalTable: "UnitFlags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UnitUnitFlags_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ArchetypePropertyModifiers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ArchetypeId = table.Column<int>(type: "integer", nullable: false),
                    PropertyModifierId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArchetypePropertyModifiers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ArchetypePropertyModifiers_Archetypes_ArchetypeId",
                        column: x => x.ArchetypeId,
                        principalTable: "Archetypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ArchetypePropertyModifiers_PropertyModifiers_PropertyModifi~",
                        column: x => x.PropertyModifierId,
                        principalTable: "PropertyModifiers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ItemItemFlags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ItemId = table.Column<int>(type: "integer", nullable: false),
                    ItemFlagId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemItemFlags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemItemFlags_ItemFlags_ItemFlagId",
                        column: x => x.ItemFlagId,
                        principalTable: "ItemFlags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ItemItemFlags_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LootTableItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    LootTableId = table.Column<int>(type: "integer", nullable: false),
                    ItemId = table.Column<int>(type: "integer", nullable: false),
                    StackMin = table.Column<int>(type: "integer", nullable: false),
                    StackMax = table.Column<int>(type: "integer", nullable: false),
                    Weight = table.Column<int>(type: "integer", nullable: false),
                    IsGuaranteed = table.Column<bool>(type: "boolean", nullable: false),
                    IsEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    IsUnique = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LootTableItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LootTableItems_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LootTableItems_LootTables_LootTableId",
                        column: x => x.LootTableId,
                        principalTable: "LootTables",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BehaviourBehaviourFlags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BehaviourId = table.Column<int>(type: "integer", nullable: false),
                    BehaviourFlagId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BehaviourBehaviourFlags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BehaviourBehaviourFlags_BehaviourFlags_BehaviourFlagId",
                        column: x => x.BehaviourFlagId,
                        principalTable: "BehaviourFlags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EffectBehaviourFlags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EffectId = table.Column<int>(type: "integer", nullable: false),
                    BehaviourFlagId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EffectBehaviourFlags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EffectBehaviourFlags_BehaviourFlags_BehaviourFlagId",
                        column: x => x.BehaviourFlagId,
                        principalTable: "BehaviourFlags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BehaviourBehaviours",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ParentId = table.Column<int>(type: "integer", nullable: false),
                    RelatedId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BehaviourBehaviours", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BehaviourPropertyModifiers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BehaviourId = table.Column<int>(type: "integer", nullable: false),
                    PropertyModifierId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BehaviourPropertyModifiers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BehaviourPropertyModifiers_PropertyModifiers_PropertyModifi~",
                        column: x => x.PropertyModifierId,
                        principalTable: "PropertyModifiers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BehaviourStatusFlags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BehaviourId = table.Column<int>(type: "integer", nullable: false),
                    StatusFlagId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BehaviourStatusFlags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BehaviourStatusFlags_StatusFlags_StatusFlagId",
                        column: x => x.StatusFlagId,
                        principalTable: "StatusFlags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Effects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Type = table.Column<string>(type: "text", nullable: true),
                    OriginParticlesPrefab = table.Column<string>(type: "text", nullable: true),
                    TargetParticlesPrefab = table.Column<string>(type: "text", nullable: true),
                    IsImmediate = table.Column<bool>(type: "boolean", nullable: false),
                    DamageType = table.Column<string>(type: "text", nullable: true),
                    DamageFormula = table.Column<string>(type: "text", nullable: true),
                    ApplyBehaviourStackCount = table.Column<int>(type: "integer", nullable: true),
                    ApplyBehaviourBehaviourId = table.Column<int>(type: "integer", nullable: true),
                    RemoveBehaviourBehaviourId = table.Column<int>(type: "integer", nullable: true),
                    ProjectileId = table.Column<int>(type: "integer", nullable: true),
                    ProjectileEffectId = table.Column<int>(type: "integer", nullable: true),
                    SearchAreaTargetLimit = table.Column<int>(type: "integer", nullable: true),
                    SearchAreaRadius = table.Column<float>(type: "real", nullable: true),
                    SearchAreaValidatorId = table.Column<int>(type: "integer", nullable: true),
                    SearchAreaEffectId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Effects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Effects_Effects_ProjectileEffectId",
                        column: x => x.ProjectileEffectId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Effects_Effects_SearchAreaEffectId",
                        column: x => x.SearchAreaEffectId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Effects_Projectiles_ProjectileId",
                        column: x => x.ProjectileId,
                        principalTable: "Projectiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EffectEffects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ParentId = table.Column<int>(type: "integer", nullable: false),
                    RelatedId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EffectEffects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EffectEffects_Effects_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EffectEffects_Effects_RelatedId",
                        column: x => x.RelatedId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "I18nEffects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    I18nId = table.Column<int>(type: "integer", nullable: false),
                    EffectId = table.Column<int>(type: "integer", nullable: false),
                    Placeholder = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_I18nEffects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_I18nEffects_Effects_EffectId",
                        column: x => x.EffectId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_I18nEffects_I18n_I18nId",
                        column: x => x.I18nId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Skills",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NameId = table.Column<int>(type: "integer", nullable: true),
                    DescriptionId = table.Column<int>(type: "integer", nullable: true),
                    EffectId = table.Column<int>(type: "integer", nullable: true),
                    Icon = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skills", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Skills_Effects_EffectId",
                        column: x => x.EffectId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Skills_I18n_DescriptionId",
                        column: x => x.DescriptionId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Skills_I18n_NameId",
                        column: x => x.NameId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Weapons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Type = table.Column<string>(type: "text", nullable: true),
                    Prefab = table.Column<string>(type: "text", nullable: true),
                    Range = table.Column<float>(type: "real", nullable: false),
                    CooldownTime = table.Column<float>(type: "real", nullable: false),
                    PreparationTime = table.Column<float>(type: "real", nullable: false),
                    IsCharging = table.Column<bool>(type: "boolean", nullable: false),
                    ChargeDurationSeconds = table.Column<float>(type: "real", nullable: false),
                    EffectId = table.Column<int>(type: "integer", nullable: true),
                    ProjectileId = table.Column<int>(type: "integer", nullable: true),
                    OriginParticlesPrefab = table.Column<string>(type: "text", nullable: true),
                    TargetParticlesPrefab = table.Column<string>(type: "text", nullable: true),
                    BeamPrefab = table.Column<string>(type: "text", nullable: true),
                    BeamReflectionCount = table.Column<int>(type: "integer", nullable: false),
                    ShotSpreadCount = table.Column<int>(type: "integer", nullable: false),
                    ShotSpreadDegrees = table.Column<float>(type: "real", nullable: false),
                    ShotBarrageCount = table.Column<int>(type: "integer", nullable: false),
                    ShotBarrageInterval = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Weapons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Weapons_Effects_EffectId",
                        column: x => x.EffectId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Weapons_Projectiles_ProjectileId",
                        column: x => x.ProjectileId,
                        principalTable: "Projectiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SkillSkillFlags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SkillId = table.Column<int>(type: "integer", nullable: false),
                    SkillFlagId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SkillSkillFlags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SkillSkillFlags_SkillFlag_SkillFlagId",
                        column: x => x.SkillFlagId,
                        principalTable: "SkillFlag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SkillSkillFlags_Skills_SkillId",
                        column: x => x.SkillId,
                        principalTable: "Skills",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SkillSkillTags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SkillId = table.Column<int>(type: "integer", nullable: false),
                    SkillTagId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SkillSkillTags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SkillSkillTags_Skills_SkillId",
                        column: x => x.SkillId,
                        principalTable: "Skills",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SkillSkillTags_SkillTag_SkillTagId",
                        column: x => x.SkillTagId,
                        principalTable: "SkillTag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ItemAffixes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NameId = table.Column<int>(type: "integer", nullable: true),
                    DescriptionId = table.Column<int>(type: "integer", nullable: true),
                    Type = table.Column<string>(type: "text", nullable: true),
                    Weight = table.Column<int>(type: "integer", nullable: false),
                    BehaviourId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemAffixes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemAffixes_I18n_DescriptionId",
                        column: x => x.DescriptionId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ItemAffixes_I18n_NameId",
                        column: x => x.NameId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ItemAffixPropertyModifiers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ItemAffixId = table.Column<int>(type: "integer", nullable: false),
                    PropertyModifierId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemAffixPropertyModifiers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemAffixPropertyModifiers_ItemAffixes_ItemAffixId",
                        column: x => x.ItemAffixId,
                        principalTable: "ItemAffixes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ItemAffixPropertyModifiers_PropertyModifiers_PropertyModifi~",
                        column: x => x.PropertyModifierId,
                        principalTable: "PropertyModifiers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UnitBehaviours",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UnitId = table.Column<int>(type: "integer", nullable: false),
                    BehaviourId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnitBehaviours", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UnitBehaviours_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Validators",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Type = table.Column<string>(type: "text", nullable: true),
                    Comparator = table.Column<string>(type: "text", nullable: true),
                    Value = table.Column<float>(type: "real", nullable: true),
                    UnitId = table.Column<int>(type: "integer", nullable: true),
                    BehaviourId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Validators", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Validators_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Behaviours",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NameId = table.Column<int>(type: "integer", nullable: true),
                    DescriptionId = table.Column<int>(type: "integer", nullable: true),
                    IsPositive = table.Column<bool>(type: "boolean", nullable: false),
                    Icon = table.Column<string>(type: "text", nullable: true),
                    Type = table.Column<string>(type: "text", nullable: true),
                    Duration = table.Column<float>(type: "real", nullable: false),
                    StackCount = table.Column<int>(type: "integer", nullable: false),
                    BuffTickPeriod = table.Column<float>(type: "real", nullable: false),
                    BuffApplyEffectId = table.Column<int>(type: "integer", nullable: true),
                    BuffTickEffectId = table.Column<int>(type: "integer", nullable: true),
                    BuffExpireEffectId = table.Column<int>(type: "integer", nullable: true),
                    BuffRemoveEffectId = table.Column<int>(type: "integer", nullable: true),
                    OnTakeDamageEffectId = table.Column<int>(type: "integer", nullable: true),
                    OnDealDamageEffectId = table.Column<int>(type: "integer", nullable: true),
                    OnKillEffectId = table.Column<int>(type: "integer", nullable: true),
                    OnDeathEffectId = table.Column<int>(type: "integer", nullable: true),
                    AuraBehaviourId = table.Column<int>(type: "integer", nullable: true),
                    AuraValidatorId = table.Column<int>(type: "integer", nullable: true),
                    AuraRadius = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Behaviours", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Behaviours_Behaviours_AuraBehaviourId",
                        column: x => x.AuraBehaviourId,
                        principalTable: "Behaviours",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Behaviours_Effects_BuffApplyEffectId",
                        column: x => x.BuffApplyEffectId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Behaviours_Effects_BuffExpireEffectId",
                        column: x => x.BuffExpireEffectId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Behaviours_Effects_BuffRemoveEffectId",
                        column: x => x.BuffRemoveEffectId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Behaviours_Effects_BuffTickEffectId",
                        column: x => x.BuffTickEffectId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Behaviours_Effects_OnDealDamageEffectId",
                        column: x => x.OnDealDamageEffectId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Behaviours_Effects_OnDeathEffectId",
                        column: x => x.OnDeathEffectId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Behaviours_Effects_OnKillEffectId",
                        column: x => x.OnKillEffectId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Behaviours_Effects_OnTakeDamageEffectId",
                        column: x => x.OnTakeDamageEffectId,
                        principalTable: "Effects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Behaviours_I18n_DescriptionId",
                        column: x => x.DescriptionId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Behaviours_I18n_NameId",
                        column: x => x.NameId,
                        principalTable: "I18n",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Behaviours_Validators_AuraValidatorId",
                        column: x => x.AuraValidatorId,
                        principalTable: "Validators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ValidatorStatusFlags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ValidatorId = table.Column<int>(type: "integer", nullable: false),
                    StatusFlagId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ValidatorStatusFlags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ValidatorStatusFlags_StatusFlags_StatusFlagId",
                        column: x => x.StatusFlagId,
                        principalTable: "StatusFlags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ValidatorStatusFlags_Validators_ValidatorId",
                        column: x => x.ValidatorId,
                        principalTable: "Validators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ValidatorUnitFlags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ValidatorId = table.Column<int>(type: "integer", nullable: false),
                    UnitFlagId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ValidatorUnitFlags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ValidatorUnitFlags_UnitFlags_UnitFlagId",
                        column: x => x.UnitFlagId,
                        principalTable: "UnitFlags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ValidatorUnitFlags_Validators_ValidatorId",
                        column: x => x.ValidatorId,
                        principalTable: "Validators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ValidatorValidators",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ParentId = table.Column<int>(type: "integer", nullable: false),
                    RelatedId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ValidatorValidators", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ValidatorValidators_Validators_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Validators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ValidatorValidators_Validators_RelatedId",
                        column: x => x.RelatedId,
                        principalTable: "Validators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArchetypePropertyModifiers_ArchetypeId",
                table: "ArchetypePropertyModifiers",
                column: "ArchetypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ArchetypePropertyModifiers_PropertyModifierId",
                table: "ArchetypePropertyModifiers",
                column: "PropertyModifierId");

            migrationBuilder.CreateIndex(
                name: "IX_Archetypes_NameId",
                table: "Archetypes",
                column: "NameId");

            migrationBuilder.CreateIndex(
                name: "IX_BehaviourBehaviourFlags_BehaviourFlagId",
                table: "BehaviourBehaviourFlags",
                column: "BehaviourFlagId");

            migrationBuilder.CreateIndex(
                name: "IX_BehaviourBehaviourFlags_BehaviourId",
                table: "BehaviourBehaviourFlags",
                column: "BehaviourId");

            migrationBuilder.CreateIndex(
                name: "IX_BehaviourBehaviours_ParentId",
                table: "BehaviourBehaviours",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_BehaviourBehaviours_RelatedId",
                table: "BehaviourBehaviours",
                column: "RelatedId");

            migrationBuilder.CreateIndex(
                name: "IX_BehaviourPropertyModifiers_BehaviourId",
                table: "BehaviourPropertyModifiers",
                column: "BehaviourId");

            migrationBuilder.CreateIndex(
                name: "IX_BehaviourPropertyModifiers_PropertyModifierId",
                table: "BehaviourPropertyModifiers",
                column: "PropertyModifierId");

            migrationBuilder.CreateIndex(
                name: "IX_Behaviours_AuraBehaviourId",
                table: "Behaviours",
                column: "AuraBehaviourId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Behaviours_AuraValidatorId",
                table: "Behaviours",
                column: "AuraValidatorId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Behaviours_BuffApplyEffectId",
                table: "Behaviours",
                column: "BuffApplyEffectId");

            migrationBuilder.CreateIndex(
                name: "IX_Behaviours_BuffExpireEffectId",
                table: "Behaviours",
                column: "BuffExpireEffectId");

            migrationBuilder.CreateIndex(
                name: "IX_Behaviours_BuffRemoveEffectId",
                table: "Behaviours",
                column: "BuffRemoveEffectId");

            migrationBuilder.CreateIndex(
                name: "IX_Behaviours_BuffTickEffectId",
                table: "Behaviours",
                column: "BuffTickEffectId");

            migrationBuilder.CreateIndex(
                name: "IX_Behaviours_DescriptionId",
                table: "Behaviours",
                column: "DescriptionId");

            migrationBuilder.CreateIndex(
                name: "IX_Behaviours_NameId",
                table: "Behaviours",
                column: "NameId");

            migrationBuilder.CreateIndex(
                name: "IX_Behaviours_OnDealDamageEffectId",
                table: "Behaviours",
                column: "OnDealDamageEffectId");

            migrationBuilder.CreateIndex(
                name: "IX_Behaviours_OnDeathEffectId",
                table: "Behaviours",
                column: "OnDeathEffectId");

            migrationBuilder.CreateIndex(
                name: "IX_Behaviours_OnKillEffectId",
                table: "Behaviours",
                column: "OnKillEffectId");

            migrationBuilder.CreateIndex(
                name: "IX_Behaviours_OnTakeDamageEffectId",
                table: "Behaviours",
                column: "OnTakeDamageEffectId");

            migrationBuilder.CreateIndex(
                name: "IX_BehaviourStatusFlags_BehaviourId",
                table: "BehaviourStatusFlags",
                column: "BehaviourId");

            migrationBuilder.CreateIndex(
                name: "IX_BehaviourStatusFlags_StatusFlagId",
                table: "BehaviourStatusFlags",
                column: "StatusFlagId");

            migrationBuilder.CreateIndex(
                name: "IX_EffectBehaviourFlags_BehaviourFlagId",
                table: "EffectBehaviourFlags",
                column: "BehaviourFlagId");

            migrationBuilder.CreateIndex(
                name: "IX_EffectBehaviourFlags_EffectId",
                table: "EffectBehaviourFlags",
                column: "EffectId");

            migrationBuilder.CreateIndex(
                name: "IX_EffectEffects_ParentId",
                table: "EffectEffects",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_EffectEffects_RelatedId",
                table: "EffectEffects",
                column: "RelatedId");

            migrationBuilder.CreateIndex(
                name: "IX_Effects_ApplyBehaviourBehaviourId",
                table: "Effects",
                column: "ApplyBehaviourBehaviourId");

            migrationBuilder.CreateIndex(
                name: "IX_Effects_ProjectileEffectId",
                table: "Effects",
                column: "ProjectileEffectId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Effects_ProjectileId",
                table: "Effects",
                column: "ProjectileId");

            migrationBuilder.CreateIndex(
                name: "IX_Effects_RemoveBehaviourBehaviourId",
                table: "Effects",
                column: "RemoveBehaviourBehaviourId");

            migrationBuilder.CreateIndex(
                name: "IX_Effects_SearchAreaEffectId",
                table: "Effects",
                column: "SearchAreaEffectId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Effects_SearchAreaValidatorId",
                table: "Effects",
                column: "SearchAreaValidatorId");

            migrationBuilder.CreateIndex(
                name: "IX_I18nEffects_EffectId",
                table: "I18nEffects",
                column: "EffectId");

            migrationBuilder.CreateIndex(
                name: "IX_I18nEffects_I18nId",
                table: "I18nEffects",
                column: "I18nId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemAffixes_BehaviourId",
                table: "ItemAffixes",
                column: "BehaviourId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemAffixes_DescriptionId",
                table: "ItemAffixes",
                column: "DescriptionId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemAffixes_NameId",
                table: "ItemAffixes",
                column: "NameId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemAffixPropertyModifiers_ItemAffixId",
                table: "ItemAffixPropertyModifiers",
                column: "ItemAffixId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemAffixPropertyModifiers_PropertyModifierId",
                table: "ItemAffixPropertyModifiers",
                column: "PropertyModifierId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemCategories_NameId",
                table: "ItemCategories",
                column: "NameId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemCategoryItemTypes_ItemCategoryId",
                table: "ItemCategoryItemTypes",
                column: "ItemCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemCategoryItemTypes_ItemTypeId",
                table: "ItemCategoryItemTypes",
                column: "ItemTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemItemFlags_ItemFlagId",
                table: "ItemItemFlags",
                column: "ItemFlagId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemItemFlags_ItemId",
                table: "ItemItemFlags",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_DescriptionId",
                table: "Items",
                column: "DescriptionId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_ItemTypeId",
                table: "Items",
                column: "ItemTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_NameId",
                table: "Items",
                column: "NameId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_RarityId",
                table: "Items",
                column: "RarityId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemTypes_NameId",
                table: "ItemTypes",
                column: "NameId");

            migrationBuilder.CreateIndex(
                name: "IX_LootTableItems_ItemId",
                table: "LootTableItems",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_LootTableItems_LootTableId",
                table: "LootTableItems",
                column: "LootTableId");

            migrationBuilder.CreateIndex(
                name: "IX_LootTableLootTables_ParentId",
                table: "LootTableLootTables",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_LootTableLootTables_RelatedId",
                table: "LootTableLootTables",
                column: "RelatedId");

            migrationBuilder.CreateIndex(
                name: "IX_LootTableRandomItems_ItemCategoryId",
                table: "LootTableRandomItems",
                column: "ItemCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_LootTableRandomItems_LootTableId",
                table: "LootTableRandomItems",
                column: "LootTableId");

            migrationBuilder.CreateIndex(
                name: "IX_LootTableRandomItems_RarityId",
                table: "LootTableRandomItems",
                column: "RarityId");

            migrationBuilder.CreateIndex(
                name: "IX_Properties_DescriptionId",
                table: "Properties",
                column: "DescriptionId");

            migrationBuilder.CreateIndex(
                name: "IX_Properties_NameId",
                table: "Properties",
                column: "NameId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyModifiers_PropertyId",
                table: "PropertyModifiers",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_Rarities_NameId",
                table: "Rarities",
                column: "NameId");

            migrationBuilder.CreateIndex(
                name: "IX_Skills_DescriptionId",
                table: "Skills",
                column: "DescriptionId");

            migrationBuilder.CreateIndex(
                name: "IX_Skills_EffectId",
                table: "Skills",
                column: "EffectId");

            migrationBuilder.CreateIndex(
                name: "IX_Skills_NameId",
                table: "Skills",
                column: "NameId");

            migrationBuilder.CreateIndex(
                name: "IX_SkillSkillFlags_SkillFlagId",
                table: "SkillSkillFlags",
                column: "SkillFlagId");

            migrationBuilder.CreateIndex(
                name: "IX_SkillSkillFlags_SkillId",
                table: "SkillSkillFlags",
                column: "SkillId");

            migrationBuilder.CreateIndex(
                name: "IX_SkillSkillTags_SkillId",
                table: "SkillSkillTags",
                column: "SkillId");

            migrationBuilder.CreateIndex(
                name: "IX_SkillSkillTags_SkillTagId",
                table: "SkillSkillTags",
                column: "SkillTagId");

            migrationBuilder.CreateIndex(
                name: "IX_SkillTag_NameId",
                table: "SkillTag",
                column: "NameId");

            migrationBuilder.CreateIndex(
                name: "IX_UnitBehaviours_BehaviourId",
                table: "UnitBehaviours",
                column: "BehaviourId");

            migrationBuilder.CreateIndex(
                name: "IX_UnitBehaviours_UnitId",
                table: "UnitBehaviours",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_Units_ArchetypeId",
                table: "Units",
                column: "ArchetypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Units_NameId",
                table: "Units",
                column: "NameId");

            migrationBuilder.CreateIndex(
                name: "IX_UnitUnitFlags_UnitFlagId",
                table: "UnitUnitFlags",
                column: "UnitFlagId");

            migrationBuilder.CreateIndex(
                name: "IX_UnitUnitFlags_UnitId",
                table: "UnitUnitFlags",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_Validators_BehaviourId",
                table: "Validators",
                column: "BehaviourId");

            migrationBuilder.CreateIndex(
                name: "IX_Validators_UnitId",
                table: "Validators",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_ValidatorStatusFlags_StatusFlagId",
                table: "ValidatorStatusFlags",
                column: "StatusFlagId");

            migrationBuilder.CreateIndex(
                name: "IX_ValidatorStatusFlags_ValidatorId",
                table: "ValidatorStatusFlags",
                column: "ValidatorId");

            migrationBuilder.CreateIndex(
                name: "IX_ValidatorUnitFlags_UnitFlagId",
                table: "ValidatorUnitFlags",
                column: "UnitFlagId");

            migrationBuilder.CreateIndex(
                name: "IX_ValidatorUnitFlags_ValidatorId",
                table: "ValidatorUnitFlags",
                column: "ValidatorId");

            migrationBuilder.CreateIndex(
                name: "IX_ValidatorValidators_ParentId",
                table: "ValidatorValidators",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_ValidatorValidators_RelatedId",
                table: "ValidatorValidators",
                column: "RelatedId");

            migrationBuilder.CreateIndex(
                name: "IX_Weapons_EffectId",
                table: "Weapons",
                column: "EffectId");

            migrationBuilder.CreateIndex(
                name: "IX_Weapons_ProjectileId",
                table: "Weapons",
                column: "ProjectileId");

            migrationBuilder.AddForeignKey(
                name: "FK_BehaviourBehaviourFlags_Behaviours_BehaviourId",
                table: "BehaviourBehaviourFlags",
                column: "BehaviourId",
                principalTable: "Behaviours",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EffectBehaviourFlags_Effects_EffectId",
                table: "EffectBehaviourFlags",
                column: "EffectId",
                principalTable: "Effects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BehaviourBehaviours_Behaviours_ParentId",
                table: "BehaviourBehaviours",
                column: "ParentId",
                principalTable: "Behaviours",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BehaviourBehaviours_Behaviours_RelatedId",
                table: "BehaviourBehaviours",
                column: "RelatedId",
                principalTable: "Behaviours",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BehaviourPropertyModifiers_Behaviours_BehaviourId",
                table: "BehaviourPropertyModifiers",
                column: "BehaviourId",
                principalTable: "Behaviours",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BehaviourStatusFlags_Behaviours_BehaviourId",
                table: "BehaviourStatusFlags",
                column: "BehaviourId",
                principalTable: "Behaviours",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Effects_Behaviours_ApplyBehaviourBehaviourId",
                table: "Effects",
                column: "ApplyBehaviourBehaviourId",
                principalTable: "Behaviours",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Effects_Behaviours_RemoveBehaviourBehaviourId",
                table: "Effects",
                column: "RemoveBehaviourBehaviourId",
                principalTable: "Behaviours",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Effects_Validators_SearchAreaValidatorId",
                table: "Effects",
                column: "SearchAreaValidatorId",
                principalTable: "Validators",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemAffixes_Behaviours_BehaviourId",
                table: "ItemAffixes",
                column: "BehaviourId",
                principalTable: "Behaviours",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UnitBehaviours_Behaviours_BehaviourId",
                table: "UnitBehaviours",
                column: "BehaviourId",
                principalTable: "Behaviours",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Validators_Behaviours_BehaviourId",
                table: "Validators",
                column: "BehaviourId",
                principalTable: "Behaviours",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Units_Archetypes_ArchetypeId",
                table: "Units");

            migrationBuilder.DropForeignKey(
                name: "FK_Behaviours_I18n_DescriptionId",
                table: "Behaviours");

            migrationBuilder.DropForeignKey(
                name: "FK_Behaviours_I18n_NameId",
                table: "Behaviours");

            migrationBuilder.DropForeignKey(
                name: "FK_Units_I18n_NameId",
                table: "Units");

            migrationBuilder.DropForeignKey(
                name: "FK_Effects_Behaviours_ApplyBehaviourBehaviourId",
                table: "Effects");

            migrationBuilder.DropForeignKey(
                name: "FK_Effects_Behaviours_RemoveBehaviourBehaviourId",
                table: "Effects");

            migrationBuilder.DropForeignKey(
                name: "FK_Validators_Behaviours_BehaviourId",
                table: "Validators");

            migrationBuilder.DropTable(
                name: "ArchetypePropertyModifiers");

            migrationBuilder.DropTable(
                name: "BehaviourBehaviourFlags");

            migrationBuilder.DropTable(
                name: "BehaviourBehaviours");

            migrationBuilder.DropTable(
                name: "BehaviourPropertyModifiers");

            migrationBuilder.DropTable(
                name: "BehaviourStatusFlags");

            migrationBuilder.DropTable(
                name: "Configuration");

            migrationBuilder.DropTable(
                name: "EffectBehaviourFlags");

            migrationBuilder.DropTable(
                name: "EffectEffects");

            migrationBuilder.DropTable(
                name: "I18nEffects");

            migrationBuilder.DropTable(
                name: "ItemAffixPropertyModifiers");

            migrationBuilder.DropTable(
                name: "ItemCategoryItemTypes");

            migrationBuilder.DropTable(
                name: "ItemItemFlags");

            migrationBuilder.DropTable(
                name: "LootTableItems");

            migrationBuilder.DropTable(
                name: "LootTableLootTables");

            migrationBuilder.DropTable(
                name: "LootTableRandomItems");

            migrationBuilder.DropTable(
                name: "SkillSkillFlags");

            migrationBuilder.DropTable(
                name: "SkillSkillTags");

            migrationBuilder.DropTable(
                name: "UnitBehaviours");

            migrationBuilder.DropTable(
                name: "UnitUnitFlags");

            migrationBuilder.DropTable(
                name: "ValidatorStatusFlags");

            migrationBuilder.DropTable(
                name: "ValidatorUnitFlags");

            migrationBuilder.DropTable(
                name: "ValidatorValidators");

            migrationBuilder.DropTable(
                name: "Weapons");

            migrationBuilder.DropTable(
                name: "BehaviourFlags");

            migrationBuilder.DropTable(
                name: "ItemAffixes");

            migrationBuilder.DropTable(
                name: "PropertyModifiers");

            migrationBuilder.DropTable(
                name: "ItemFlags");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "ItemCategories");

            migrationBuilder.DropTable(
                name: "LootTables");

            migrationBuilder.DropTable(
                name: "SkillFlag");

            migrationBuilder.DropTable(
                name: "Skills");

            migrationBuilder.DropTable(
                name: "SkillTag");

            migrationBuilder.DropTable(
                name: "StatusFlags");

            migrationBuilder.DropTable(
                name: "UnitFlags");

            migrationBuilder.DropTable(
                name: "Properties");

            migrationBuilder.DropTable(
                name: "ItemTypes");

            migrationBuilder.DropTable(
                name: "Rarities");

            migrationBuilder.DropTable(
                name: "Archetypes");

            migrationBuilder.DropTable(
                name: "I18n");

            migrationBuilder.DropTable(
                name: "Behaviours");

            migrationBuilder.DropTable(
                name: "Effects");

            migrationBuilder.DropTable(
                name: "Projectiles");

            migrationBuilder.DropTable(
                name: "Validators");

            migrationBuilder.DropTable(
                name: "Units");
        }
    }
}
