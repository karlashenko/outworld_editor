using System;
using System.Collections.Generic;
using System.Linq;
using Outworld.Data.Types;
using Outworld.Editor.Database.Entities;

namespace Outworld.Editor.ViewModels
{
    public class PropertyModifiersViewModel : ViewModel<PropertyModifier>
    {
        public List<Property> Properties;

        public readonly string[] ModifierTypes = Enum.GetValues(typeof(ModifierType)).Cast<ModifierType>().Select(x => x.ToString()).ToArray();
    }
}