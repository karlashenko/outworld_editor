using System;
using System.Collections.Generic;
using System.Linq;
using Outworld.Data.Types;
using Outworld.Editor.Database.Entities;

namespace Outworld.Editor.ViewModels
{
    public class BehaviourViewModel : ViewModel<Behaviour>
    {
        public List<BehaviourFlag> Flags;
        public List<StatusFlag> StatusFlags;
        public List<Validator> Validators;
        public List<Behaviour> Behaviours;
        public List<PropertyModifier> PropertyModifiers;
        public List<Effect> Effects;

        public readonly string[] BehaviourTypes = Enum.GetValues(typeof(BehaviourType)).Cast<BehaviourType>().Select(x => x.ToString()).ToArray();
    }
}