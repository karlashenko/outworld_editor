using System;
using System.Collections.Generic;
using System.Linq;
using Outworld.Data.Types;
using Outworld.Editor.Database.Entities;

namespace Outworld.Editor.ViewModels
{
    public class WeaponViewModel : ViewModel<Weapon>
    {
        public IEnumerable<Effect> Effects;
        public IEnumerable<Projectile> Projectiles;

        public readonly string[] WeaponTypes = Enum.GetValues(typeof(WeaponType)).Cast<WeaponType>().Select(x => x.ToString()).ToArray();
    }
}