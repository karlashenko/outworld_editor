using System.Collections.Generic;
using Outworld.Editor.Database.Entities;

namespace Outworld.Editor.ViewModels
{
    public class ViewLibrary
    {
        public IEnumerable<I18n> I18n;
        public IEnumerable<string> Icons;
        public IEnumerable<string> Prefabs;
    }
}