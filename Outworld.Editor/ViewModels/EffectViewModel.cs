using System;
using System.Collections.Generic;
using System.Linq;
using Outworld.Data.Types;
using Outworld.Editor.Database.Entities;

namespace Outworld.Editor.ViewModels
{
    public class EffectViewModel : ViewModel<Effect>
    {
        public List<Effect> Effects;
        public List<Behaviour> Behaviours;
        public List<Projectile> Projectiles;
        public List<Validator> Validators;
        public List<BehaviourFlag> BehaviourFlags;

        public readonly string[] EffectTypes = Enum.GetValues(typeof(EffectType)).Cast<EffectType>().Select(x => x.ToString()).ToArray();
        public readonly string[] DamageTypes = Enum.GetValues(typeof(DamageType)).Cast<DamageType>().Select(x => x.ToString()).ToArray();
    }
}