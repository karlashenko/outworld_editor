namespace Outworld.Editor.ViewModels.Partial
{
    public class FormButtonViewModel
    {
        public readonly string Action;
        public readonly string Label;
        public readonly string Class;
        public readonly string Icon;

        public FormButtonViewModel(string action, string label, string @class, string icon = "")
        {
            this.Action = action;
            this.Label = label;
            this.Class = @class;
            this.Icon = icon;
        }
    }
}