using System.Collections.Generic;

namespace Outworld.Editor.ViewModels.Partial
{
    public class RelationFormViewModel
    {
        public readonly string Title;
        public readonly int ParentId;
        public readonly string ActionSuffix;
        public readonly IEnumerable<Related> RelatedViewModels;

        public RelationFormViewModel(int parentId, string title, string actionSuffix, IEnumerable<Related> relatedViewModels)
        {
            this.Title = title;
            this.ParentId = parentId;
            this.ActionSuffix = actionSuffix;
            this.RelatedViewModels = relatedViewModels;
        }

        public class Related
        {
            public readonly int Id;
            public readonly string PrimaryKeyName;
            public readonly string ParentKeyName;
            public readonly IEnumerable<RelatedProperty> Properties;

            public Related(int id, string primaryKeyName, string parentKeyName, IEnumerable<RelatedProperty> properties)
            {
                this.Id = id;
                this.PrimaryKeyName = primaryKeyName;
                this.ParentKeyName = parentKeyName;
                this.Properties = properties;
            }
        }

        public class RelatedProperty
        {
            public readonly string Label;
            public readonly string View;
            public readonly object ViewModel;

            public RelatedProperty(string label, string view, object viewModel)
            {
                this.Label = label;
                this.View = view;
                this.ViewModel = viewModel;
            }
        }
    }
}