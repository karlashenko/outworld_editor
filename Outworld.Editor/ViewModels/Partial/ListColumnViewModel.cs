namespace Outworld.Editor.ViewModels.Partial
{
    public class ListColumnViewModel
    {
        public string Key;
        public string Label;
        public int Width;

        public ListColumnViewModel(string key, int width)
        {
            this.Key = key;
            this.Label = key;
            this.Width = width;
        }
    }
}