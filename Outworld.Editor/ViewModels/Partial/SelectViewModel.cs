using System.Collections.Generic;

namespace Outworld.Editor.ViewModels.Partial
{
    public class SelectViewModel
    {
        public class Option
        {
            public readonly object Value;
            public readonly string Label;

            public Option(object value)
            {
                this.Label = value.ToString() ?? "";
                this.Value = value;
            }

            public Option(object value, string label)
            {
                this.Value = value;
                this.Label = label;
            }
        }

        public readonly string Name;
        public readonly string Label;
        public readonly object Value;
        public readonly IEnumerable<Option> Options;

        public SelectViewModel(string name, object value, IEnumerable<Option> options)
        {
            this.Name = name;
            this.Label = "";
            this.Value = value;
            this.Options = options;
        }

        public SelectViewModel(string label, string name, object value, IEnumerable<Option> options)
        {
            this.Label = label;
            this.Name = name;
            this.Value = value;
            this.Options = options;
        }
    }
}