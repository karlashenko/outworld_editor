namespace Outworld.Editor.ViewModels.Partial
{
    public class InputViewModel
    {
        public readonly string Name;
        public readonly string Label;
        public readonly string Type;
        public readonly object? Value;

        public InputViewModel(string name, object value)
        {
            this.Name = name;
            this.Value = value;
            this.Label = "";
            this.Type = "text";
        }

        public InputViewModel(string label, string name, object value)
        {
            this.Label = label;
            this.Name = name;
            this.Value = value;
            this.Type = "text";
        }

        public InputViewModel(string label, string name, string type, object value)
        {
            this.Label = label;
            this.Name = name;
            this.Type = type;
            this.Value = value;
        }
    }
}