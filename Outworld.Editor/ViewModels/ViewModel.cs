using System.Collections.Generic;
using Outworld.Editor.Database.Entities;

namespace Outworld.Editor.ViewModels
{
    public class ViewModel<T> where T : IIdentity
    {
        public T Active;
        public IEnumerable<T> Models;
    }
}