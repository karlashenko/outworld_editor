using System.Collections.Generic;
using Outworld.Editor.Database.Entities;

namespace Outworld.Editor.ViewModels
{
    public class ItemCategoriesViewModel : ViewModel<ItemCategory>
    {
        public List<ItemType> ItemTypes;
    }
}