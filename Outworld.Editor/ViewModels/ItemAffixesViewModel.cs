using System.Collections.Generic;
using Outworld.Editor.Database.Entities;

namespace Outworld.Editor.ViewModels
{
    public class ItemAffixesViewModel : ViewModel<ItemAffix>
    {
        public List<PropertyModifier> PropertyModifiers;
    }
}