using System.Collections.Generic;
using Outworld.Editor.Database.Entities;

namespace Outworld.Editor.ViewModels
{
    // ReSharper disable once InconsistentNaming
    public class I18nViewModel : ViewModel<I18n>
    {
        public List<Effect> Effects;
    }
}