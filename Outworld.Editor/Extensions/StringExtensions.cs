using System.Text.RegularExpressions;

namespace Outworld.Editor.Extensions
{
    public static class StringExtensions
    {
        public static bool LikeIgnoreCase(this string toSearch, string toFind)
        {
            return toSearch.ToLower().Like(toFind.ToLower());
        }

        public static bool Like(this string toSearch, string toFind)
        {
            return new Regex(
                @"\A" + new Regex(@"\.|\$|\^|\{|\[|\(|\||\)|\*|\+|\?|\\")
                    .Replace(toFind, ch => @"\" + ch)
                    .Replace('_', '.')
                    .Replace("%", ".*") + @"\z",
                RegexOptions.Singleline
            ).IsMatch(toSearch);
        }
    }
}