using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Outworld.Editor.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static bool IsActiveRoute(this IHtmlHelper<dynamic> html, string controller, string action)
        {
            var route = html.ViewContext.RouteData;

            return (string) route.Values["action"] == action &&
                   (string) route.Values["controller"] == controller;
        }

        public static string ActiveRouteClassAny(this IHtmlHelper<dynamic> html, params string[] controllers)
        {
            return controllers.Any(controller => html.IsActiveRoute(controller, "Index")) ? "active" : "";
        }

        public static string ActiveRouteClass(this IHtmlHelper<dynamic> html, string controller, string action = "Index")
        {
            return html.IsActiveRoute(controller, action) ? "active" : "";
        }

        public static string Link(this IHtmlHelper<dynamic> html, string label, string controller, string action)
        {
            var active = html.ActiveRouteClass(controller, action);
            return $"<a asp-controller=\"{controller}\" asp-action=\"{action}\" class=\"nav-link text-dark {active}\">{label}</a>";
        }
    }
}