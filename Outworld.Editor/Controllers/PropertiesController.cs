using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class PropertiesController : Controller<Property, ViewModel<Property>>
    {
        public PropertiesController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }

        protected override IQueryable<Property> OnQuery(IQueryable<Property> query)
        {
            return query
                .Include(model => model.Name)
                .Include(model => model.Description);
        }

        protected override string SearchString(Property model)
        {
            return $"{model.Type}{model.Name?.En}{model.Description?.En}";
        }
    }
}