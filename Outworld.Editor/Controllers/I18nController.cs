using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

// ReSharper disable InconsistentNaming

namespace Outworld.Editor.Controllers
{
    public class I18nController : Controller<I18n, I18nViewModel>
    {
        public I18nController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }

        public IActionResult SaveEffects(int id, I18nEffect[] effects)
        {
            var i18n = this.Context.I18n.Include(a => a.Effects).First(a => a.Id == id);
            i18n.Effects = effects.ToList();
            this.Context.SaveChanges();

            return RedirectToIndex(i18n);
        }

        public IActionResult AddEffects(int id)
        {
            var i18n = this.Context.I18n.Include(a => a.Effects).AsEnumerable().First(a => a.Id == id);
            var effect = this.Context.Effects.AsEnumerable().First();
            i18n.Effects.Add(new I18nEffect {I18nId = id, EffectId = effect.Id});
            this.Context.SaveChanges();

            return RedirectToIndex(i18n);
        }

        public IActionResult DeleteEffects(int id, int relatedId)
        {
            var i18n = this.Context.I18n.Include(a => a.Effects).First(a => a.Id == id);
            var i18NEffect = i18n.Effects.First(ap => ap.Id == relatedId);
            i18n.Effects.Remove(i18NEffect);
            this.Context.SaveChanges();

            return RedirectToIndex(i18n);
        }

        protected override IQueryable<I18n> OnQuery(IQueryable<I18n> query)
        {
            return query.Include(model => model.Effects).OrderBy(model => model.Key);
        }

        protected override void OnViewModelCreated(I18nViewModel viewModel)
        {
            viewModel.Effects = this.Context.Effects.ToList();
        }

        protected override string SearchString(I18n model)
        {
            return $"{model.Key}{model.En}";
        }
    }
}