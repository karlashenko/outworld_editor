using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class ItemCategoriesController : Controller<ItemCategory, ItemCategoriesViewModel>
    {
        public ItemCategoriesController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }

        public IActionResult SaveItemTypes(int id, ItemCategoryItemType[] itemTypes)
        {
            var category = this.Context.ItemCategories.Include(a => a.Types).First(a => a.Id == id);
            category.Types = itemTypes.ToList();
            this.Context.SaveChanges();

            return RedirectToIndex(category);
        }

        public IActionResult AddItemTypes(int id)
        {
            var category = this.Context.ItemCategories.Include(a => a.Types).First(a => a.Id == id);
            var itemType = this.Context.ItemTypes.AsEnumerable().First();
            category.Types.Add(new ItemCategoryItemType {ItemCategoryId = category.Id, ItemTypeId = itemType.Id});
            this.Context.SaveChanges();

            return RedirectToIndex(category);
        }

        public IActionResult DeleteItemTypes(int id, int relatedId)
        {
            var category = this.Context.ItemCategories.Include(a => a.Types).First(a => a.Id == id);
            category.Types.Remove(category.Types.First(ap => ap.Id == relatedId));
            this.Context.SaveChanges();

            return RedirectToIndex(category);
        }

        protected override void OnViewModelCreated(ItemCategoriesViewModel viewModel)
        {
            viewModel.ItemTypes = this.Context.ItemTypes.Include(type => type.Name).ToList();
        }

        protected override IQueryable<ItemCategory> OnQuery(IQueryable<ItemCategory> query)
        {
            return query
                .Include(category => category.Types)
                .ThenInclude(type => type.ItemType)
                .ThenInclude(type => type.Name);
        }
    }
}