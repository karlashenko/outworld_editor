using Microsoft.AspNetCore.Hosting;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class SkillsController : Controller<Skill, ViewModel<Skill>>
    {
        public SkillsController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }
    }
}