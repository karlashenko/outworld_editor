using Microsoft.AspNetCore.Hosting;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class BehaviourFlagsController : Controller<BehaviourFlag, ViewModel<BehaviourFlag>>
    {
        public BehaviourFlagsController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }
    }
}