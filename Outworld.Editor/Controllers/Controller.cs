using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.Extensions;
using Outworld.Editor.ViewModels;
using MVCController = Microsoft.AspNetCore.Mvc.Controller;

namespace Outworld.Editor.Controllers
{
    public class Controller<TModel, TViewModel> : MVCController
        where TModel : class, IIdentity, new()
        where TViewModel : ViewModel<TModel>, new()
    {
        protected readonly DatabaseContext Context;
        protected readonly IWebHostEnvironment Environment;

        public Controller(DatabaseContext context, IWebHostEnvironment environment)
        {
            this.Context = context;
            this.Environment = environment;
        }

        [HttpGet]
        public IActionResult Index(int? id)
        {
            var search = HttpContext.Request.Query["search"].ToString();

            var models = OnQuery(this.Context.Set<TModel>())
                .OrderBy(model => model.Id)
                .AsEnumerable()
                .Where(model => string.IsNullOrEmpty(search) || SearchPredicate(model, search))
                .ToList();

            if (id == null)
            {
                var first = models.FirstOrDefault();

                if (first != null)
                {
                    return RedirectToIndex(first);
                }
            }

            var viewModel = new TViewModel();
            viewModel.Active = this.Context.Find<TModel>(id)!;
            viewModel.Models = models;

            OnViewModelCreated(viewModel);

            var viewLibrary = new ViewLibrary();
            viewLibrary.Icons = Directory.GetFiles(Path.Combine(this.Environment.WebRootPath, "resources/Sprites/Items"), "*.png").Select(Path.GetFileName)!;
            viewLibrary.Prefabs = Directory.GetFiles(Path.Combine(this.Environment.WebRootPath, "resources/Prefabs"), "*.prefab", SearchOption.AllDirectories).Select(Path.GetFileName)!;
            viewLibrary.I18n = this.Context.I18n;
            ViewData["Library"] = viewLibrary;

            return View("Index", viewModel);
        }

        [HttpPost]
        public IActionResult Create()
        {
            var model = new TModel();

            this.Context.Add(model);
            this.Context.SaveChanges();

            return RedirectToIndex(model);
        }

        [HttpPost]
        public IActionResult Duplicate(TModel model)
        {
            model.Id = 0;

            this.Context.Add(model);
            this.Context.SaveChanges();

            return RedirectToIndex(model);
        }

        [HttpPost]
        public IActionResult Update(TModel model)
        {
            this.Context.Update(model);
            this.Context.SaveChanges();

            return RedirectToIndex(model);
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            var model = this.Context.Find<TModel>(id);

            if (model == null)
            {
                return NotFound();
            }

            this.Context.Remove(model);
            this.Context.SaveChanges();

            return RedirectToIndex();
        }

        protected virtual void OnViewModelCreated(TViewModel viewModel)
        {
        }

        protected virtual IQueryable<TModel> OnQuery(IQueryable<TModel> query)
        {
            return query;
        }

        protected virtual string SearchString(TModel model)
        {
            return model.Id.ToString();
        }

        protected IActionResult RedirectToIndex(IIdentity propertyContainer)
        {
            return RedirectToAction("Index", new
                {
                    id = propertyContainer.Id,
                    search = HttpContext.Request.Query["search"].ToString(),
                    filter = HttpContext.Request.Query["filter"].ToString(),
                }
            );
        }

        protected IActionResult RedirectToIndex()
        {
            return RedirectToAction("Index", new
                {
                    search = HttpContext.Request.Query["search"].ToString(),
                    filter = HttpContext.Request.Query["filter"].ToString(),
                }
            );
        }

        private bool SearchPredicate(TModel model, string query)
        {
            var result = false;
            var search = SearchString(model);

            foreach (var part in query.Split("|"))
            {
                result = result || search.LikeIgnoreCase($"%{part}%");
            }

            return result;
        }
    }
}