using Microsoft.AspNetCore.Hosting;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class RaritiesController : Controller<Rarity, ViewModel<Rarity>>
    {
        public RaritiesController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }
    }
}