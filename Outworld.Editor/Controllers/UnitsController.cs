using Microsoft.AspNetCore.Hosting;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class UnitsController : Controller<Unit, ViewModel<Unit>>
    {
        public UnitsController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }
    }
}