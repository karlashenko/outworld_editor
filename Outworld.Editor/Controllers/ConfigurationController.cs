using Microsoft.AspNetCore.Hosting;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class ConfigurationController : Controller<Configuration, ViewModel<Configuration>>
    {
        public ConfigurationController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }
    }
}