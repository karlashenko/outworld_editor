using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class ItemAffixesController : Controller<ItemAffix, ItemAffixesViewModel>
    {
        public ItemAffixesController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }

        public IActionResult SavePropertyModifiers(int id, ItemAffixPropertyModifier[] propertyModifiers)
        {
            var affix = this.Context.ItemAffixes.Include(a => a.PropertyModifiers).First(a => a.Id == id);
            affix.PropertyModifiers = propertyModifiers.ToList();
            this.Context.SaveChanges();

            return RedirectToIndex(affix);
        }

        public IActionResult AddPropertyModifiers(int id)
        {
            var affix = this.Context.ItemAffixes.Include(a => a.PropertyModifiers).AsEnumerable().First(a => a.Id == id);
            var propertyModifier = this.Context.PropertyModifiers.AsEnumerable().First();
            affix.PropertyModifiers.Add(new ItemAffixPropertyModifier {ItemAffixId = affix.Id, PropertyModifierId = propertyModifier.Id});
            this.Context.SaveChanges();

            return RedirectToIndex(affix);
        }

        public IActionResult DeletePropertyModifiers(int id, int relatedId)
        {
            var affix = this.Context.ItemAffixes.Include(a => a.PropertyModifiers).First(a => a.Id == id);
            affix.PropertyModifiers.Remove(affix.PropertyModifiers.First(ap => ap.Id == relatedId));
            this.Context.SaveChanges();

            return RedirectToIndex(affix);
        }

        protected override void OnViewModelCreated(ItemAffixesViewModel viewModel)
        {
            viewModel.PropertyModifiers = this.Context.PropertyModifiers
                .Include(modifier => modifier.Property)
                .ThenInclude(modifier => modifier!.Name)
                .ToList();
        }

        protected override IQueryable<ItemAffix> OnQuery(IQueryable<ItemAffix> query)
        {
            return query
                .Include(model => model.Name)
                .Include(model => model.Description)
                .Include(model => model.PropertyModifiers)
                .ThenInclude(modifier => modifier.PropertyModifier)
                .ThenInclude(modifier => modifier.Property)
                .ThenInclude(property => property!.Name);
        }

        protected override string SearchString(ItemAffix model)
        {
            return $"{model.Type}{model.Name?.En}{model.Description?.En}";
        }
    }
}