using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Outworld.Data;
using Outworld.Data.Flags;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;

namespace Outworld.Editor.Controllers
{
    public class DownloadController : Controller
    {
        private readonly DatabaseContext context;

        public DownloadController(DatabaseContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var behaviours = this.context.Behaviours
                .Include(x => x.Flags)
                .ThenInclude(x => x.BehaviourFlag)
                .Include(x => x.StatusFlags)
                .ThenInclude(x => x.StatusFlag)
                .Select(Map)
                .ToList();

            var dataset = new Dataset
            {
                Items = this.context.Items.Select(Map).ToList(),
                ItemTypes = this.context.ItemTypes.Select(Map).ToList(),
                Weapon = this.context.Weapons.Select(Map).ToList(),
                Projectiles = this.context.Projectiles.Select(Map).ToList(),
                I18n = this.context.I18n.Select(Map).ToList(),
                Behaviours = behaviours,
            };

            return Content(JsonConvert.SerializeObject(dataset), "application/json");
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public IActionResult I18n(string locale)
        {
            var dictionary = new Dictionary<string, string>();

            foreach (var i18n in this.context.I18n)
            {
                dictionary.Add(i18n.Key!, i18n[locale].ToString()!);
            }

            return Json(dictionary);
        }

        private static BehaviourData Map(Behaviour entity)
        {
            return new BehaviourData
            {
                Id = entity.Id,
                Flags = ConvertEnum<BehaviourFlags>(string.Join(", ", entity.Flags.Select(x => x.BehaviourFlag.Name)))
            };
        }

        private static T? ConvertEnum<T>(string serialized, T? fallback = default)
        {
            try
            {
                return (T) Enum.Parse(typeof(T), serialized);
            }
            catch (Exception exception)
            {
                // ignored
            }

            return fallback;
        }

        private static ItemData Map(Item entity)
        {
            return new ItemData();
        }

        private static WeaponData Map(Weapon entity)
        {
            return new WeaponData();
        }

        private static ProjectileData Map(Projectile entity)
        {
            return new ProjectileData();
        }

        private static ItemTypeData Map(ItemType entity)
        {
            return new ItemTypeData();
        }

        private static I18nData Map(I18n entity)
        {
            return new I18nData();
        }
    }
}