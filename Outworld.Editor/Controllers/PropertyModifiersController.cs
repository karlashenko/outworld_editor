using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class PropertyModifiersController : Controller<PropertyModifier, PropertyModifiersViewModel>
    {
        public PropertyModifiersController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }

        protected override void OnViewModelCreated(PropertyModifiersViewModel viewModel)
        {
            viewModel.Properties = this.Context.Properties.Include(property => property.Name).ToList();
        }
    }
}