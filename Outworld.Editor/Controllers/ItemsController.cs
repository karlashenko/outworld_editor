using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class ItemsController : Controller<Item, ViewModel<Item>>
    {
        public ItemsController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }

        protected override IQueryable<Item> OnQuery(IQueryable<Item> query)
        {
            return query
                .Include(model => model.Name)
                .Include(model => model.Description);
        }

        protected override string SearchString(Item model)
        {
            return $"{model.Id}{model.Name?.En}";
        }
    }
}