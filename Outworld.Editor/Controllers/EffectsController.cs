using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class EffectsController : Controller<Effect, EffectViewModel>
    {
        public EffectsController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }

        protected override void OnViewModelCreated(EffectViewModel viewModel)
        {
            viewModel.Behaviours = this.Context.Behaviours.Include(x => x.Name).ToList();
            viewModel.Effects = this.Context.Effects.ToList();
            viewModel.Projectiles = this.Context.Projectiles.ToList();
            viewModel.Validators = this.Context.Validators.ToList();
        }
    }
}