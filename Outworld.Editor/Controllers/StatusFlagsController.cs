using Microsoft.AspNetCore.Hosting;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class StatusFlagsController : Controller<StatusFlag, ViewModel<StatusFlag>>
    {
        public StatusFlagsController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }
    }
}