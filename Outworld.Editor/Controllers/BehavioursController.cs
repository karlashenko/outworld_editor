using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class BehavioursController : Controller<Behaviour, BehaviourViewModel>
    {
        public BehavioursController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }

        protected override void OnViewModelCreated(BehaviourViewModel viewModel)
        {
            viewModel.Flags = this.Context.BehaviourFlags.ToList();
            viewModel.StatusFlags = this.Context.StatusFlags.ToList();
            viewModel.Validators = this.Context.Validators.ToList();
            viewModel.Behaviours = this.Context.Behaviours.Include(x => x.Name).ToList();
            viewModel.Effects = this.Context.Effects.ToList();
            viewModel.PropertyModifiers = this.Context.PropertyModifiers.ToList();
        }

        protected override IQueryable<Behaviour> OnQuery(IQueryable<Behaviour> query)
        {
            return query
                .Include(model => model.Flags)
                .Include(model => model.StatusFlags)
                .Include(model => model.Behaviours)
                .Include(model => model.PropertyModifiers)
                .OrderBy(model => model.Id);
        }

        public IActionResult SaveFlags(int id, BehaviourBehaviourFlag[] flags)
        {
            var behaviour = this.Context.Behaviours.Include(x => x.Flags).First(x => x.Id == id);
            behaviour.Flags = flags.ToList();
            this.Context.SaveChanges();

            return RedirectToIndex(behaviour);
        }

        public IActionResult AddFlags(int id)
        {
            var behaviour = this.Context.Behaviours.Include(x => x.Flags).First(x => x.Id == id);
            var behaviourFlag = this.Context.BehaviourFlags.AsEnumerable().First();

            behaviour.Flags.Add(new BehaviourBehaviourFlag {BehaviourId = id, BehaviourFlagId = behaviourFlag.Id});
            this.Context.SaveChanges();

            return RedirectToIndex(behaviour);
        }

        public IActionResult DeleteFlags(int id, int relatedId)
        {
            var behaviour = this.Context.Behaviours.Include(x => x.Flags).First(x => x.Id == id);
            var behaviourFlag = behaviour.Flags.First(x => x.Id == relatedId);

            behaviour.Flags.Remove(behaviourFlag);
            this.Context.SaveChanges();

            return RedirectToIndex(behaviour);
        }

        public IActionResult SaveStatusFlags(int id, BehaviourStatusFlag[] statusFlags)
        {
            var behaviour = this.Context.Behaviours.Include(x => x.StatusFlags).First(x => x.Id == id);
            behaviour.StatusFlags = statusFlags.ToList();
            this.Context.SaveChanges();

            return RedirectToIndex(behaviour);
        }

        public IActionResult AddStatusFlags(int id)
        {
            var behaviour = this.Context.Behaviours.Include(x => x.StatusFlags).First(x => x.Id == id);
            var statusFlag = this.Context.StatusFlags.AsEnumerable().First();

            behaviour.StatusFlags.Add(new BehaviourStatusFlag {BehaviourId = id, StatusFlagId = statusFlag.Id});
            this.Context.SaveChanges();

            return RedirectToIndex(behaviour);
        }

        public IActionResult DeleteStatusFlags(int id, int relatedId)
        {
            var behaviour = this.Context.Behaviours.Include(x => x.StatusFlags).First(a => a.Id == id);
            var behaviourStatusFlag = behaviour.StatusFlags.First(x => x.Id == relatedId);

            behaviour.StatusFlags.Remove(behaviourStatusFlag);
            this.Context.SaveChanges();

            return RedirectToIndex(behaviour);
        }

        public IActionResult SaveBehaviours(int id, BehaviourBehaviour[] behaviours)
        {
            var behaviour = this.Context.Behaviours.Include(x => x.Behaviours).First(x => x.Id == id);
            behaviour.Behaviours = behaviours.ToList();
            this.Context.SaveChanges();

            return RedirectToIndex(behaviour);
        }

        public IActionResult AddBehaviours(int id)
        {
            var behaviour = this.Context.Behaviours.Include(x => x.Behaviours).First(x => x.Id == id);

            behaviour.Behaviours.Add(new BehaviourBehaviour() {ParentId = id, RelatedId = behaviour.Id});
            this.Context.SaveChanges();

            return RedirectToIndex(behaviour);
        }

        public IActionResult DeleteBehaviours(int id, int relatedId)
        {
            var behaviour = this.Context.Behaviours.Include(x => x.Behaviours).First(a => a.Id == id);
            var related = behaviour.Behaviours.First(x => x.Id == relatedId);

            behaviour.Behaviours.Remove(related);
            this.Context.SaveChanges();

            return RedirectToIndex(behaviour);
        }

        public IActionResult SavePropertyModifiers(int id, BehaviourPropertyModifier[] propertyModifiers)
        {
            var behaviour = this.Context.Behaviours.Include(x => x.PropertyModifiers).First(x => x.Id == id);
            behaviour.PropertyModifiers = propertyModifiers.ToList();
            this.Context.SaveChanges();

            return RedirectToIndex(behaviour);
        }

        public IActionResult AddPropertyModifiers(int id)
        {
            var behaviour = this.Context.Behaviours.Include(x => x.PropertyModifiers).First(x => x.Id == id);
            var propertyModifier = this.Context.PropertyModifiers.First();

            behaviour.PropertyModifiers.Add(new BehaviourPropertyModifier {BehaviourId = id, PropertyModifierId = propertyModifier.Id});
            this.Context.SaveChanges();

            return RedirectToIndex(behaviour);
        }

        public IActionResult DeletePropertyModifiers(int id, int relatedId)
        {
            var behaviour = this.Context.Behaviours.Include(x => x.PropertyModifiers).First(a => a.Id == id);
            var propertyModifier = behaviour.PropertyModifiers.First(x => x.Id == relatedId);

            behaviour.PropertyModifiers.Remove(propertyModifier);
            this.Context.SaveChanges();

            return RedirectToIndex(behaviour);
        }
    }
}