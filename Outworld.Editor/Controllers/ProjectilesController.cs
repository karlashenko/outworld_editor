using Microsoft.AspNetCore.Hosting;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class ProjectilesController : Controller<Projectile, ViewModel<Projectile>>
    {
        public ProjectilesController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }
    }
}