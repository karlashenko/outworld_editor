using Microsoft.AspNetCore.Hosting;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class WeaponController : Controller<Weapon, WeaponViewModel>
    {
        public WeaponController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }

        protected override void OnViewModelCreated(WeaponViewModel viewModel)
        {
            viewModel.Effects = this.Context.Effects;
            viewModel.Projectiles = this.Context.Projectiles;
        }
    }
}