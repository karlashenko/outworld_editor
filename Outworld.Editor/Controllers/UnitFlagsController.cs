using Microsoft.AspNetCore.Hosting;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class UnitFlagsController : Controller<UnitFlag, ViewModel<UnitFlag>>
    {
        public UnitFlagsController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }
    }
}