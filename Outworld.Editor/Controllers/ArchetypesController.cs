using Microsoft.AspNetCore.Hosting;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class ArchetypesController : Controller<Archetype, ViewModel<Archetype>>
    {
        public ArchetypesController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }
    }
}