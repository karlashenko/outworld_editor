using Microsoft.AspNetCore.Hosting;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class ItemTypesController : Controller<ItemType, ViewModel<ItemType>>
    {
        public ItemTypesController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }
    }
}