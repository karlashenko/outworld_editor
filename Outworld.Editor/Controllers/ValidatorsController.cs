using Microsoft.AspNetCore.Hosting;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class ValidatorsController : Controller<Validator, ViewModel<Validator>>
    {
        public ValidatorsController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }
    }
}