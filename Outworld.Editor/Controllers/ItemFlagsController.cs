using Microsoft.AspNetCore.Hosting;
using Outworld.Editor.Database;
using Outworld.Editor.Database.Entities;
using Outworld.Editor.ViewModels;

namespace Outworld.Editor.Controllers
{
    public class ItemFlagsController : Controller<ItemFlag, ViewModel<ItemFlag>>
    {
        public ItemFlagsController(DatabaseContext context, IWebHostEnvironment environment) : base(context, environment)
        {
        }
    }
}